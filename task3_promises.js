const express = require('express');
const app = express();
const request = require('request');
const promise = require('bluebird');
const helpers = require('./helpers');
const morgan = require('morgan');
app.use(morgan('combined'));
app.get('/I/want/title/', function(req, res){
    let queryString = [];
    let titles = [];
    if (Array.isArray(req.query.address))
        queryString = req.query.address;
    else queryString.push(req.query.address);
    
    if (!queryString)
        return res.status(200).json({status: false, code: 404, message: 'addresses not found'});

    let tasks = [];
    queryString.forEach(url => {
        tasks.push(new Promise((resolve, reject) => {
            fethcTitle(url).then((results) =>{
                titles.push({
                    'url': results.url,
                    'title': results.title
                });
                resolve();
            });
        }));
    });
    const startTime = new Date().getTime();
    return Promise.all(tasks).then(() => {
        const requestTime = new Date().getTime() - startTime;
        return res.send(helpers.compileResults({'titles': titles, 'time': requestTime}));
    }).catch((err) => {
        console.log(err)
        return res.send('Some error occured on Server');
    });  
});
function validateUrl(url) {
    const pattern = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return pattern.test(url);
}
function fethcTitle(url) {
    return new Promise ((resolve, reject) => {
        if(!validateUrl(url)) {
            resolve ({
                status : true,
                'title': 'NO RESPONSE',
                'url': url
            });
        } else {
            let title;
            request(url, function (error, response, body) {
                if (error) {
                    resolve ({
                        status : true,
                        'title': 'NO RESPONSE',
                        'url': url
                    });
                }
                title = body.match(/<title[^>]*>([^<]+)<\/title>/)[1];
                resolve ({status : true, 'title': title, 'url': url});
            });
        } 
    });  
}

//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
  res.send('Route not found', 404);
});

app.listen(3000, '127.0.0.1');