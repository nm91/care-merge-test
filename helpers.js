const Handlebars = require('handlebars');
module.exports = {
    compileResults: function(results) {
        console.log(results.titles)
        let source = "<h1>Following are the titles of given websites.</h1>" +
        "<h3>Request processed in {{time}}ms.</h3>" + 
        "<ul>{{#sites}}<li>{{url}} - {{title}}</li>{{/sites}}</ul>";
        const template = Handlebars.compile(source);
        const data = { "time": results.time, "sites": results.titles};
        console.log(data);
        return template(data);
    }
};