const express = require('express');
const app = express();
const request = require('request');
const helpers = require('./helpers');
const morgan = require('morgan');
app.use(morgan('combined'));
app.get('/I/want/title/', function(req, res){
    let queryString = [];
    if (Array.isArray(req.query.address))
        queryString = req.query.address;
    else queryString.push(req.query.address);
    
    if (!queryString)
        return res.status(200).json({status: false, code: 404, message: 'addresses not found'});

    let titles =  [];
    queryString.forEach(function(url, index) {
        const startTime = new Date().getTime();
        fethcTitle(url, function(err, title) {
            if (err) {
                console.log(err);
            }
            else titles.push(title);
            if(titles.length == queryString.length) {
                const requestTime = new Date().getTime() - startTime;
                const results = helpers.compileResults({'titles': titles, 'time': requestTime});
                return res.send(results);
            }
        });
    });
    
    
});

function validateUrl(url) {
    let pattern = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return pattern.test(url);
}
function fethcTitle(url, callback) {
    if(!validateUrl(url)) {
        return callback(null, {
            'url': url,
            'title': 'NO RESPONSE'
        })
    } else {
        request(url, function (error, response, body) {
            let title = body.match(/<title[^>]*>([^<]+)<\/title>/)[1];
            return callback(null, {
                'url': url,
                'title': title
            });
        });
    }
}
//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
  res.send('Route not found', 404);
});

app.listen(3000, '127.0.0.1');