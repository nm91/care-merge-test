const express = require('express');
const app = express();
const request = require('request');
const async = require('async');
const helpers = require('./helpers');
const morgan = require('morgan');
app.use(morgan('combined'));
app.get('/I/want/title/', function(req, res){
    let queryString = [];
    let titles = [];
    if (Array.isArray(req.query.address))
        queryString = req.query.address;
    else queryString.push(req.query.address);
    
    if (!queryString)
        return res.status(200).json({status: false, code: 404, message: 'addresses not found'});
    
    let tasks = [];
    queryString.forEach(url => {
        tasks.push((callback)=> {
            fethcTitle(url, function(results) {
                titles.push({
                    'url': results.url,
                    'title': results.title
                });
                return callback(null);
            });
        });
    });
    const startTime = new Date().getTime();
    async.parallelLimit(tasks, 6, (err, result) => {
        if (err) {
            return console.log(err);
        }
        const requestTime = new Date().getTime() - startTime;
        return res.send(helpers.compileResults({'titles': titles, 'time': requestTime}));
    });

    
    
});
function validateUrl(url) {
    const pattern = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
    return pattern.test(url);
}
function fethcTitle(url, callback) {
    let title;
    if(!validateUrl(url)) {
        return callback ({status : true, 'title': 'NO RESPONSE', 'url': url});
    } else {
        request(url, function (error, response, body) {
            if (error) {
                callback({status: false});
            }
            title = body.match(/<title[^>]*>([^<]+)<\/title>/)[1];
            return callback ({status : true, 'title': title, 'url': url});
        });
    }
}

//The 404 Route (ALWAYS Keep this as the last route)
app.get('*', function(req, res){
  res.send('Route not found', 404);
});

app.listen(3000, '127.0.0.1');